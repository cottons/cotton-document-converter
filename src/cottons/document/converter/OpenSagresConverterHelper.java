package cottons.document.converter;

import cottons.document.converter.utils.Logger;
import fr.opensagres.xdocreport.converter.*;
import fr.opensagres.xdocreport.core.document.DocumentKind;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;

public class OpenSagresConverterHelper implements DocumentConverter {

	@Override
	public InputStream odtToPdf(InputStream odtStream) {
		try {
			return toPdf(odtStream, Options.getFrom(DocumentKind.ODT).to(ConverterTypeTo.PDF));
		} catch (XDocConverterException e) {
			Logger.log().severe(e.getMessage());
			return null;
		}
	}

	@Override
	public InputStream docxToPdf(InputStream docxStream) {
		try {
			return toPdf(docxStream, Options.getFrom(DocumentKind.DOCX).to(ConverterTypeTo.PDF));
		} catch (XDocConverterException e) {
			Logger.log().severe(e.getMessage());
			return null;
		}
	}

	private static InputStream toPdf(InputStream stream, Options options) throws XDocConverterException {
		IConverter converter = ConverterRegistry.getRegistry().getConverter(options);
		ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
		converter.convert(stream, outputStream, options);
		return new ByteArrayInputStream(outputStream.toByteArray());
	}

}

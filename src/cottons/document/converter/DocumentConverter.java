package cottons.document.converter;

import cottons.utils.MimeTypes;

import java.io.InputStream;
import java.net.URL;

public interface DocumentConverter {
	InputStream odtToPdf(InputStream odtStream);
	InputStream docxToPdf(InputStream docxStream);

	default InputStream toPdf(URL document) throws Exception {
		return toPdf(document.openStream());
	}

	default InputStream toPdf(InputStream stream) throws Exception {
		String extension = MimeTypes.getExtension(MimeTypes.getFromStream(stream));
		stream.reset();
		if (extension.contains("odt")) return odtToPdf(stream);
		if (extension.contains("docx")) return docxToPdf(stream);
		if (extension.contains("pdf")) return stream;
		return null;
	}
}

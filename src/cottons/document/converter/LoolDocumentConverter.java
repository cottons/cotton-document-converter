package cottons.document.converter;

import cottons.document.converter.utils.Logger;
import io.intino.konos.alexandria.schema.Resource;
import io.intino.konos.restful.RestfulApi;
import io.intino.konos.restful.core.RestfulAccessor;
import io.intino.konos.restful.exceptions.RestfulFailure;

import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;

public class LoolDocumentConverter implements DocumentConverter {
	private final String documentEditorServiceUrl;

	private static final String ConverterPath = "/lool/convert-to/%s";

	public LoolDocumentConverter(String serviceUrl) {
		this.documentEditorServiceUrl = serviceUrl;
	}

	@Override
	public InputStream odtToPdf(InputStream odtStream) {
		return documentToPdf(odtStream);
	}

	@Override
	public InputStream docxToPdf(InputStream docxStream) {
		return documentToPdf(docxStream);
	}

	private InputStream documentToPdf(InputStream stream) {
		RestfulApi api = new RestfulAccessor();
		try {
			RestfulApi.Response response = api.post(loolUrl(), String.format(ConverterPath, "pdf"), new Resource("data").data(stream));
			return response.contentAsStream();
		} catch (RestfulFailure error) {
			Logger.log().severe(error.getMessage());
			return null;
		}
	}

	private URL loolUrl() {
		try {
			return new URL(documentEditorServiceUrl);
		} catch (MalformedURLException e) {
			return null;
		}
	}
}
